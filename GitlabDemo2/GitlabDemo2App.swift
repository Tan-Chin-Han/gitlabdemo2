//
//  GitlabDemo2App.swift
//  GitlabDemo2
//
//  Created by 蔡龍賢 on 2022/7/8.
//

import SwiftUI

@main
struct GitlabDemo2App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
