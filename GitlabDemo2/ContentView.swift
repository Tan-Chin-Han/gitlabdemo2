//
//  ContentView.swift
//  GitlabDemo2
//
//  Created by 蔡龍賢 on 2022/7/8.
//

import SwiftUI

struct ContentView: View {
    
    @State private var title = "Hello, world!"
    
    @State private var age = ""
    @State private var name = ""
    
    var body: some View {
        VStack {
            Text(title)
                .padding()
            TextField("name", text: $name)
                .padding()
                .accessibilityIdentifier("nameTextField")
            TextField("age", text: $age)
                .padding()
                .accessibilityIdentifier("ageTextField")
            Button {
                title = "Hello, Jerry"
            } label: {
                Text("Save")
            }.accessibilityIdentifier("saveButton")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
